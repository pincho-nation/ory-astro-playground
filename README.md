# Basic setup PoC

- Redirects unauthenticated users to `/login` which in turn redirects to local proxy running on port `3004`. This double redirect might be redundant but came up during playing around and I haven't bothered trying to remove it, as it's kinda nice to have control over the redirection via the local api.
- After registering and/or logging in returns back to localhost:3000/ and displays some session-identity based information
- Renders two <Assortment /> components, one which, if the tuples below are set up, will be authorized to fetch a mock-json file, and another which will be unauthorized.

*The setup is only tested via Ory Cloud.*

## Requirements

**Run Ory proxy on port 3004**

```
npm i -g @ory/cli
ory tunnel --project {projectSlug} --dev http://localhost:3000/ --port 3004
```

**Environment**

Create `.env` in the root of the project with `ORY_API_URL` and `ORY_PAT` exported.

```
ORY_API_URL="https://{projectSlug}.projects.oryapis.com"
ORY_PAT="ory_pat_{ACCESS_TOKEN}}"
```

**The following relationship tuples are assumed in Keto**

```json
[
    {
        "action": "insert",
        "relation_tuple": {
            "subject_id": {{your registered user session.identity.id}},
            "relation": "service-user",
            "namespace": "organizations",
            "object": "headquarters"
        }
    },
    {
        "action": "insert",
        "relation_tuple": {
            "subject_set": {
                "relation": "service-user",
                "namespace": "organizations",
                "object": "headquarters"
            },
            "relation": "service-user",
            "namespace": "venues",
            "object": "se-hq-it"
        }
    },
    {
        "action": "insert",
        "relation_tuple": {
            "subject_set": {
                "relation": "service-user",
                "namespace": "venues",
                "object": "se-hq-it"
            },
            "relation": "assortment:read",
            "namespace": "venues",
            "object": "se-hq-it"
        }
    }
]
```

**Run the Astro dev server**

```
npm run dev
```
