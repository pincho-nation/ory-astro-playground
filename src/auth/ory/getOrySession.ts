import type { Session } from "@ory/client";
import { authClient } from "./client";

export const getOrySession = async (req: Request): Promise<Session> => {
  const cookie = req.headers.get("cookie") || undefined;
  const { data: session } = await authClient.toSession(undefined, cookie);
  return session;
};
