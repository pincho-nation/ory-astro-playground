import { V0alpha2Api, Configuration, ReadApi, RelationQuery, GetCheckResponse } from "@ory/client";

export const authClient = new V0alpha2Api(
  new Configuration({
    basePath: "http://localhost:3004",
    baseOptions: {
      withCredentials: true,
    },
  })
);

function initReadClient(): ReadApi {
  const config = new Configuration({
    basePath: import.meta.env.ORY_API_URL,
    accessToken: import.meta.env.ORY_PAT,
    apiKey: import.meta.env.ORY_PAT,
  });
  const readClient = new ReadApi(config);
  return readClient;
}

const readClient = initReadClient();

export const keto = {
  check: async (relationQuery: RelationQuery): Promise<GetCheckResponse> => {
    const response = await readClient.postCheck(undefined, relationQuery);
    return response.data;
  },
};
