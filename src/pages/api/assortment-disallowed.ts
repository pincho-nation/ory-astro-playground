import { authClient, keto } from "../../auth/ory/client";
import assortment from "./assortment.json";

// Resturn assortment json if user is authorized against ory keto on localhost:3004
export async function get({ request }: { request: Request }) {
  const { data: session } = await authClient.toSession(
    undefined,
    request.headers.get("cookie") || undefined
  );

  try {
    const response = await keto.check({
      namespace: "venues",
      object: "se-hq-it",
      relation: "assortment:some-action-we-arent-allowed-to-do",
      subject_id: session.identity.id,
    });
    console.log("response", response); // { data: { result: true } }
    if (response.allowed) {
      return {
        body: JSON.stringify(assortment),
      };
    } else {
      return new Response("Unauthorized", { status: 401, statusText: "Unauthorized" });
    }
  } catch (error: any) {
    console.error(error);
  }
}
